// import {Stage} from '@serenity-js/core/lib/stage';
// import {Activity} from '@serenity-js/core/src/screenplay/activities';

// declare namespace Cypress {
//     interface Chainable<Subject> {
//         stageActorAttemptsTo(stage: Stage, actor: string, ...activities: Activity[]): Chainable<Subject>
//     }
// }
//
// function stageActorAttemptsTo(stage: Stage, actor: string, ...activities: Activity[]) : void {
//     new Cypress.Promise((resolve, reject) => stage.theActorCalled(actor).attemptsTo(
//         ...activities,
//     ).then(resolved => resolve(), rejected => reject(rejected)));
// }

declare namespace Cypress {
    interface Chainable<Subject> {
        stageActorAttemptsTo(): Chainable<Subject>
    }
}

Cypress.Commands.add("stageActorAttemptsTo", () => {console.log('TEST')});
