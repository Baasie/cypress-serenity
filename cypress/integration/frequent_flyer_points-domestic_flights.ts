import {serenity} from '@serenity-js/core/lib';
import {Travellers} from '../spec/screenplay/travellers';
import {ClickOnButton, OpenTheApplication} from '../spec/screenplay/tasks/OpenTheApplication';

describe('Authorisation', () => {

    const stage = serenity.callToStageFor(new Travellers());

    describe('Authorize for the internal NN Portal', () => {

        // it('should authorise', () => {
        //     cy.visit('/products');
        //     cy.contains('Request information').click();
        //     cy.contains('send message').should('be.visible');
        // })

        beforeEach(() => {

            // cy.stageActorAttemptsTo(stage, 'james', OpenTheApplication.on("/events"));

            cy.then(() => new Cypress.Promise((resolve, reject) => stage.theActorCalled('James').attemptsTo(
                OpenTheApplication.on("/events"),
            ).then(resolved => resolve(), rejected => reject(rejected))));});

        it('earns standard points', () => {
             cy.then(() => new Cypress.Promise((resolve, reject) => stage.theActorInTheSpotlight().attemptsTo(

                 ClickOnButton(),
               //   ClickOnButton(),
            ).then(resolved => resolve(), rejected => reject(rejected))));
            // cy.theActorCalledAndAttemptsTo();
        });

        // @ts-ignore: commands
        afterEach(() => cy.stageActorAttemptsTo());

    });
});